Upgrade
=======

v2.1.0 to v2.2.0
----------------
- Fix composer dependencies
- Add rules for WAF NAXSI on CommentRecord className

v2.0.0 to v2.1.0
----------------
- Add CI 
- Fix composer dependencies 
- Fix directory name for fixtures tests

v1.3.3 to v2.0.0
----------------
- Enable translator
- Remove unused file config

v1.3.2 to v1.3.3
----------------
- Prefix sto will be replace by sco.

v1.3.1 to v1.3.2
----------------
- Change design on submit button form.

v1.3.0 to v1.3.1
----------------
- FIX configuration DQL functions

v1.2.0 to v1.3.0
----------------
- Add the possibility to limit the list with pattern.

v1.1.0 to v1.2.0
----------------
- Add the possibility to choose a front-end cms (bootstrap or foundation)
- Add the possibily to send extra data on comment

v1.0.0 to v1.1.0
----------------
- Add the possibility to disable reply on comment.
- Fix PHPDoc
- Change design on all buttons.