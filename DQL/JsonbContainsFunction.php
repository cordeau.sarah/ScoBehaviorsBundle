<?php
/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class JsonbContainsFunction
 * @package daMMSBundle\DQL
 * @codeCoverageIgnore
 *
 * "JSONB_CONTAINS" "(" StringPrimary "," StringPrimary ")"
 * Example to used it where("SCO_JSONB_CONTAINS(r.extra, :pattern) = true")
 * where pattern = '{"sampleId":"2","failureGroupId":"16","lruId":"3134224323","modopId":"4768.56298.47545.30153"}'
 */
class JsonbContainsFunction extends FunctionNode
{
    /** @var array  */
    private $parameters = [];

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $expression = '%s ::jsonb @>  %s ::jsonb';

        return sprintf(
            $expression,
            $this->parameters['field']->dispatch($sqlWalker),
            $this->parameters['pattern']->dispatch($sqlWalker)
        );
    }

    /**
     * @param Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->parameters['field'] = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->parameters['pattern'] = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}