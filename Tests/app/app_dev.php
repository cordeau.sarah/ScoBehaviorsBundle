<?php
// Tests/app/app_dev.php

namespace Sco\BehaviorsBundle\Tests\app;

require_once __DIR__ . '../../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$kernel = new AppKernel('dev', true);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);