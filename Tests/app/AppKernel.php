<?php

namespace Sco\BehaviorsBundle\Tests\app;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Fidry\AliceDataFixtures\Bridge\Symfony\FidryAliceDataFixturesBundle;
use FOS\UserBundle\FOSUserBundle;
use Hautelook\AliceBundle\HautelookAliceBundle;
use Liip\FunctionalTestBundle\LiipFunctionalTestBundle;
use Nelmio\Alice\Bridge\Symfony\NelmioAliceBundle;
use Sco\BehaviorsBundle\ScoBehaviorsBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        return array(
            new FrameworkBundle(),
            new SecurityBundle(),
            new SwiftmailerBundle(),
            new TwigBundle(),
            new DoctrineBundle(),
            new FOSUserBundle(),
            new DoctrineFixturesBundle(),
            new NelmioAliceBundle(),
            new FidryAliceDataFixturesBundle(),
            new HautelookAliceBundle(),
            new LiipFunctionalTestBundle(),
            new ScoBehaviorsBundle()
        );
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config.yml');
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        $cacheDir = __DIR__.'/../var/cache/'.$this->getEnvironment();
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777, true);
        }

        return $cacheDir;
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        $logDir = __DIR__.'/../var/logs/'.$this->getEnvironment();
        if (!is_dir($logDir)) {
            mkdir($logDir, 0777, true);
        }

        return $logDir;
    }
}