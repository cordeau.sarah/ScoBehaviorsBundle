<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Tests\tests\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Sco\BehaviorsBundle\DependencyInjection\BehaviorsExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class BehaviorsExtensionTest
 * @package Sco\BehaviorsBundle\Tests\tests\DependencyInjection
 */
class BehaviorsExtensionTest extends TestCase
{
    public static function provideExtensions()
    {
        return array(
            array('commentable'),
        );
    }

    public function testGetAlias()
    {
        $extension = new BehaviorsExtension();
        $alias = $extension->getAlias();
        $this->assertEquals('sco_behaviors_bundle', $alias, 'Error in alias name');
    }

    /**
     * @dataProvider provideExtensions
     */
    public function testLoadConfigWithCommentable($listener)
    {
        $extension = new BehaviorsExtension();
        $container = new ContainerBuilder();
        $config = array('commentable' => true);
        $extension->load(array($config), $container);

        $this->assertTrue($container->hasDefinition(sprintf('sco.behaviors_bundle.%s_subscriber', $listener)));
    }

    /**
     * @dataProvider provideExtensions
     */
    public function testLoadConfigWithoutCommentable($listener)
    {
        $extension = new BehaviorsExtension();
        $container = new ContainerBuilder();
        $config = array('commentable' => false);
        $extension->load(array($config), $container);
        $this->assertFalse($container->hasDefinition(sprintf('sco.behaviors_bundle.%s_subscriber', $listener)));
    }
}