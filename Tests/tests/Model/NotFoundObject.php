<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Tests\tests\Model;

/**
 * Class Dummy
 * @package Sco\BehaviorsBundle\Tests\tests\Model
 */
class NotFoundObject
{
    private $id = 0;

    public function __construct($id = null)
    {
        if(null !== $id) {
            $this->id = $id;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}