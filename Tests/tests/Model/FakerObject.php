<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Tests\tests\Model;

/**
 * Class Dummy
 * @package Sco\BehaviorsBundle\Tests\tests\Model
 */
class FakerObject
{
    public function __construct()
    {
    }

    public function __toString()
    {
        return FakerObject::class;
    }
}