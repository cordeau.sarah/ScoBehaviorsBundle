<?php
/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Tests\tests;

use Doctrine\ORM\EntityManager;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Sco\BehaviorsBundle\Tests\Client;
use Sco\BehaviorsBundle\Tests\tests\Model\UserTest;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class BaseWebTestCase extends WebTestCase
{
    /* @var $client Client $client */
    protected $client;

    /** @var  Application $application */
    protected static $application;

    /** @var  ContainerInterface $container */
    protected $container;

    /** @var  EntityManager $entityManager */
    protected $entityManager;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->client = $this->makeClient();
        $this->container = $container = $this->client->getContainer();
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->loadFixtureFiles(array(
           __DIR__.'/../Fixtures/comment_record.yml',
            __DIR__.'/../Fixtures/comment.yml',
        ));
    }

    protected function logAsAnonymous()
    {
        $session = $this->client->getContainer()->get('session');

        // the firewall context (defaults to the firewall name)
        $firewall = 'main';

        $token = new UsernamePasswordToken('anon.', null, $firewall, array());
        $this->client->getContainer()->get('security.token_storage')->setToken($token);

        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    /**
     *
     */
    protected function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        // the firewall context (defaults to the firewall name)
        $firewall = 'main';

        $user = (new UserTest())->setUsername('admin')->setPlainPassword('p@ssword');

        $token = new UsernamePasswordToken($user, null, $firewall, array('ROLE_ADMIN'));
        $this->client->getContainer()->get('security.token_storage')->setToken($token);

        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}