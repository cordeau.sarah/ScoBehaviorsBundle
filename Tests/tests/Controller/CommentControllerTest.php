<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Tests\tests\Controller;

use Sco\BehaviorsBundle\Entity\Comment;
use Sco\BehaviorsBundle\Repository\CommentRepository;
use Sco\BehaviorsBundle\Tests\tests\BaseWebTestCase;
use Sco\BehaviorsBundle\Tests\tests\Model\DummyObject;
use Sco\BehaviorsBundle\Controller\CommentController;
use Sco\BehaviorsBundle\Tests\tests\Model\FakerObject;
use Sco\BehaviorsBundle\Tests\tests\Model\NotFoundObject;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentControllerTest
 * @package Sco\BehaviorsBundle\Tests\tests\Controller
 */
class CommentControllerTest extends BaseWebTestCase
{
    /** @var CommentController $controller */
    private $controller;

    /** @var CommentRepository $commentRepository */
    private $commentRepository;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        parent::setUp();

        $controller = new CommentController();
        $controller->setContainer($this->container);
        $this->controller = $controller;
        $this->commentRepository = $this->entityManager->getRepository(Comment::class);
    }

    /**
     * @group form
     */
    public function testFormActionWithoutObject()
    {
        /** @var Response $response */
        $response = $this->controller->formAction();

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('Missing argument or null passed.', $response->getContent(), 'Content not valid');
    }

    /**
     * @group form
     */
    public function testFormActionWithoutCallableGetClass()
    {
        $this->logAsAnonymous();

        $response = $this->controller->formAction(1);

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('The method get_class is not callable.', $response->getContent(), 'Faker object get_class is callable.');
    }

    /**
     * @group form
     */
    public function testFormActionWithoutCallableGetId()
    {
        $this->logAsAnonymous();

        $dummy = new FakerObject();
        $response = $this->controller->formAction($dummy);

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('The method getId is not callable.', $response->getContent(), 'Faker object getId is callable.');
    }

    /**
     * @group form
     */
    public function testFormActionWithUser()
    {
        $this->logIn();

        $dummy = new DummyObject();
        $response = $this->controller->formAction($dummy);

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('Enter your comment here...', $response->getContent(), 'Form not rendering.');
        $this->assertStringNotContainsString('Enter your username here...', $response->getContent(), 'User not found.');
    }

    /**
     * @group form
     */
    public function testFormActionWithAnonymousUser()
    {
        $this->logAsAnonymous();

        $dummy = new DummyObject();

        $response = $this->controller->formAction($dummy);
        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('Enter your comment here...', $response->getContent(), 'Form not rendering.');
        $this->assertStringContainsString('Enter your username here...', $response->getContent(), 'User anonymous not found.');
    }

    /**
     * @group list
     */
    public function testListActionWithEmptyObject()
    {
        $response = $this->controller->listAction();

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('Missing argument or null passed.', $response->getContent(), 'Content not valid');
    }

    /**
     * @group list
     */
    public function testListActionWithNoResult()
    {
        $object = new NotFoundObject();

        $response = $this->controller->listAction($object);

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringNotContainsString('sco_behaviors_bundle__comment__list__children', $response->getContent(), 'List is not empty.');
    }

    /**
     * @group list
     */
    public function testListActionWithResult()
    {
        $object = $this->getAvailableObject();

        $response = $this->controller->listAction($object, true);

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsString('sco_behaviors_bundle__comment__list__comment', $response->getContent(), 'List is empty.');
    }

    /**
     * @group new
     */
    public function testNewActionWithoutCommentRecord()
    {
        $this->logAsAnonymous();
        $object = $this->getAvailableObject();

        $response = $this->controller->formAction($object);
        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code when get form.");
	
        list(, $data) = $this->getData($response, false);
	
	    $request = new Request(['object_id' => $object->getId()], $data);
	    $response = $this->controller->newAction($request);
	    $this->assertEquals(200, $response->getStatusCode());
	    $this->assertStringContainsStringIgnoringCase('Missing argument or null passed', $response->getContent());
	
	    $request = new Request(['object_id' => $object->getId()], $data);
	    $request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
	    $response = $this->controller->newAction($request);
	    $this->assertEquals(400, $response->getStatusCode());
    }
	
	/**
	 * @group new
	 */
	public function testNewActionWithoutData()
	{
		$this->logAsAnonymous();
		$object = $this->getAvailableObject();
		
		$response = $this->controller->formAction($object);
		$this->assertEquals(200, $response->getStatusCode(), "Unexpected status code when get form.");
	    
        $request = new Request(['object_id' => $object->getId()]);
        $response = $this->controller->newAction($request);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsStringIgnoringCase('Missing argument or null passed', $response->getContent());
		
		$request = new Request(['object_id' => $object->getId()]);
		$request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
		$response = $this->controller->newAction($request);
		$this->assertEquals(400, $response->getStatusCode());
    }
	
	/**
	 * @group new
	 */
	public function testNewActionWithFormInvalid()
	{
		$this->logAsAnonymous();
		$object = $this->getAvailableObject();
		
		$response = $this->controller->formAction($object);
		$this->assertEquals(200, $response->getStatusCode(), "Unexpected status code when get form.");
		list(, $data) = $this->getData($response);
		unset($data['sco_behaviors_bundle_comment_form']['content']);
		$request = new Request(['object_id' => $object->getId()], $data);
		$request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
		$response = $this->controller->newAction($request);
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertStringContainsStringIgnoringCase('Content cannot be blank', $response->getContent());
	}
	
	/**
	 * @group new
	 */
	public function testNewAction()
	{
		$this->logAsAnonymous();
		$object = $this->getAvailableObject();
		
		$response = $this->controller->formAction($object);
		$this->assertEquals(200, $response->getStatusCode(), "Unexpected status code when get form.");
		
		list($values, $data) = $this->getData($response);
		$this->assertEquals(str_replace('\\', '\\\\', DummyObject::class), $values['sco_behaviors_bundle_comment_form[commentRecord]']);
		
		// Test json response
		$request = new Request(['object_id' => $object->getId()], $data);
		$request->headers->add(['X-Requested-With' => 'XMLHttpRequest']);
		$response = $this->controller->newAction($request);
		$this->assertEquals(200, $response->getStatusCode());
	}
	
	/**
	 * @group new
	 */
	public function testNewActionWithRedirect()
	{
		$this->logAsAnonymous();
		$object = $this->getAvailableObject();
		
		$response = $this->controller->formAction($object);
		$this->assertEquals(200, $response->getStatusCode(), "Unexpected status code when get form.");
		
		list($values, $data) = $this->getData($response);
		$this->assertEquals(str_replace('\\', '\\\\', DummyObject::class), $values['sco_behaviors_bundle_comment_form[commentRecord]']);
		
		// Test redirect
		$request = new Request(['object_id' => $object->getId()], $data);
		$request->headers->add(['referer' => 'http://localhost']);
		$response = $this->controller->newAction($request);
		$this->assertEquals(302, $response->getStatusCode());
	}
	
	/**
     * @group statistic
     */
    public function testStatistic()
    {
        $response = $this->controller->statisticAction('year', 'desc');

        $this->assertEquals(200, $response->getStatusCode(), "Unexpected status code.");
        $this->assertStringContainsStringIgnoringCase('Statistics for comment record', $response->getContent(), 'Empty statistic.');
    }

    /**
     * Return the first object find in comment
     * @return null|DummyObject
     */
    private function getAvailableObject()
    {
        $comments = $this->commentRepository->findAll();
        $object = null;
        if(count($comments)) {
            /** @var Comment $comment */
            $comment = $comments[0];
            $id = $comment->getCommentRecord()->getObjectId();
            $object = new DummyObject($id);
        }

        return $object;
    }
	
	private function getData(Response $response, bool $hasCommentRecord = true): array
	{
		$values = $this->getValues($response);
		$data['sco_behaviors_bundle_comment_form']['content'] = 'PhpUnit test';
		$data['sco_behaviors_bundle_comment_form']['createdBy'] = 'PhpUnit testeur';
		$data['sco_behaviors_bundle_comment_form']['commentRecord'] = "";
		$data['sco_behaviors_bundle_comment_form']['extra'] = $values['sco_behaviors_bundle_comment_form[extra]'];
		$data['sco_behaviors_bundle_comment_form']['_token'] = $values['sco_behaviors_bundle_comment_form[_token]'];
		
		if ($hasCommentRecord) {
			$data['sco_behaviors_bundle_comment_form']['commentRecord'] = $values['sco_behaviors_bundle_comment_form[commentRecord]'];
		}
		
		return [$values, $data];
	}
	
	private function getValues(Response $response): array
	{
		$html = '<html><body>' . $response->getContent() . '</body></html>';
		$crawler = new Crawler($html, 'http://localhost');
		$form = $crawler->filter('form[name=sco_behaviors_bundle_comment_form]')->form();
		
		return $form->getValues();
	}
}
