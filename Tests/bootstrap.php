<?php
// Test/bootstrap.php

use Doctrine\Common\Annotations\AnnotationRegistry;

if (!is_file($loaderFile = __DIR__.'/../vendor/autoload.php') && !is_file($loaderFile = __DIR__.'/../../../../../../vendor/autoload.php')) {
    throw new \LogicException('Could not find autoload.php in vendor/. Did you run "composer install --dev"?');
}

$loader = require $loaderFile;

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

use Sco\BehaviorsBundle\Tests\app\AppKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Input\ArrayInput;

use Doctrine\Bundle\DoctrineBundle\Command\DropDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\CreateDatabaseDoctrineCommand;

$kernel = new AppKernel('test', true); // create a "test" kernel
$kernel->boot();

$application = new Application($kernel);

// add the database:drop command to the application and run it
//$command = new DropDatabaseDoctrineCommand();
//$application->add($command);
//$input = new ArrayInput(array(
//    'command' => 'doctrine:database:drop',
//    '--if-exists' => true,
//    '--force' => true,
//));
//$command->run($input, new ConsoleOutput());

// add the database:create command to the application and run it
//$command = new CreateDatabaseDoctrineCommand();
//$application->add($command);
//$input = new ArrayInput(array(
//    'command' => 'doctrine:database:create',
//    'if-not-exists' => true,
//));
//$command->run($input, new ConsoleOutput());

//// add the doctrine:schema:update command to the application and run it
//$command = new CreateDatabaseDoctrineCommand();
//$application->add($command);
//$input = new ArrayInput(array(
//    'command' => 'doctrine:schema:update',
//));
//$command->run($input, new ConsoleOutput());

// add the hautelook:fixtures:load command to the application and run it
//$command = new CreateDatabaseDoctrineCommand();
//$application->add($command);
//$input = new ArrayInput(array(
//    'command' => 'hautelook:fixtures:load',
//));
//
//$command->run($input, new ConsoleOutput());