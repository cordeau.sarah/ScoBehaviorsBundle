<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Trait CommentMethods
 * @package Sco\BehaviorsBundle\Entity
 */
trait CommentMethods
{
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param mixed $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return CommentRecord
     */
    public function getCommentRecord()
    {
        return $this->commentRecord;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param CommentRecord $commentRecord
     * @return $this
     */
    public function setCommentRecord(CommentRecord $commentRecord)
    {
        $commentRecord->setComment($this);
        $this->commentRecord = $commentRecord;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param Comment $comment
     * @return $this
     */
    public function addChildren(Comment $comment)
    {
        $this->children[] = $comment;
        $comment->setParent($this);

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return Comment|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param Comment $comment
     * @return $this
     */
    public function setParent(Comment $comment)
    {
        $this->parent = $comment;

        return $this;
    }

    /**
     * @return int
     */
    public function countParent()
    {
        return $this->recursiveCountParent($this);
    }

    /**
     * @param Comment $child
     * @param int $count
     * @return int
     */
    private function recursiveCountParent(Comment &$child, $count = 0)
    {
        if(null !== $parent = $child->getParent()) {
            $count++;
            $count = $this->recursiveCountParent($parent, $count);
        }

        return $count;
    }
}