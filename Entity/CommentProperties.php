<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait CommentProperties
 * @package Sco\BehaviorsBundle\Entity
 * @codeCoverageIgnore
 */
trait CommentProperties
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var mixed
     */
    protected $createdBy;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var string
     * @Assert\NotBlank(message="Content cannot be blank")
     */
    protected $content;

    /**
     * @var CommentRecord
     * @Assert\NotNull(message="Comment record cannot be null")
     */
    private $commentRecord;

    /** @var ArrayCollection */
    private $children;

    /** @var Comment */
    private $parent;
}