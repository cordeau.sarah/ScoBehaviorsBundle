<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait CommentRecordProperties
 * @package Sco\BehaviorsBundle\Entity
 * @codeCoverageIgnore
 */
trait CommentRecordProperties
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotNull(message="Class Name cannot be null")
     * @Assert\NotBlank(message="Class Name cannot be empty")
     */
    protected $className;

    /**
     * @var integer
     * @Assert\NotNull(message="Object Id cannot be null")
     */
    protected $objectId;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var array
     */
    protected $extra;

    /**
     * @var Comment
     */
    protected $comment;
}