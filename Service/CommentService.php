<?php
/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Service;
use Doctrine\ORM\EntityManagerInterface;
use Sco\BehaviorsBundle\Entity\Comment;
use Sco\BehaviorsBundle\Entity\CommentRecord;
use Sco\BehaviorsBundle\Repository\CommentRecordRepository;
use Sco\BehaviorsBundle\Repository\CommentRepository;

class CommentService
{
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $object
     * @param string|null $pattern
     * @return Comment[]|array
     */
    public function fetchComments($object, string $pattern = null)
    {
        /** @var CommentRepository $commentRepo */
        $commentRepo = $this->em->getRepository(Comment::class);
        /** @var Comment[]|array $repository */

        $comments = $commentRepo->fetchComments($object, $pattern);

        return $comments;
    }

    /**
     * @param array $search
     * @param string $filter
     * @return array
     */
    public function fetchStatistics($search, $filter)
    {
        /** @var CommentRecordRepository $recordRepo */
        $recordRepo = $this->em->getRepository(CommentRecord::class);
        /** @var array $result */
        $result = $recordRepo->fetchStatistics($search);

        /**
         * Fetch result to display statistics
         */
        $stats = [];
        $key = strtolower($filter);

        foreach ($result as $r) {
            if(!array_key_exists($r[$key], $stats)) {
                $stats[$r[$key]][] = $r;
                continue;
            }

            $stats[$r[$key]][] = $r;
        }

        return [$key, $stats];
    }

    /**
     * @param $objectId
     * @param string $className
     * @param null $extra
     * @return CommentRecord
     */
    public function registryComment($objectId, $className, $extra = null)
    {
        /** @var CommentRecord $record */
        $record =(new CommentRecord())
            ->setObjectId($objectId)
            ->setClassName($className);

        if(null !== $extra) {
            $record->setExtra($extra);
        }

        /** @var Comment $comment */
        $comment = (new Comment())->setCommentRecord($record);

        return $record;
    }
}