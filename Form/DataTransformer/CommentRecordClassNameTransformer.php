<?php
/**
 * Project: BehaviorsBundle
 * User: stoads
 * Date: 16/03/2020
 * Time: 12:29
 */

namespace Sco\BehaviorsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class CommentRecordClassNameTransformer implements DataTransformerInterface
{
	public function transform($className): string
	{
		return str_replace('\\', '\\\\', $className);
	}
	
	public function reverseTransform($className): string
	{
		return stripslashes($className);
	}
}