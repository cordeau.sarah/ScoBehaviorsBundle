<?php

/**
 * This file is part of the ScoBehaviorsBundle package.
 *
 * (c) Sarah CORDEAU <cordeau.sarah@gmail.com>
 */

namespace Sco\BehaviorsBundle\Controller;

use Sco\BehaviorsBundle\Entity\Comment;
use Sco\BehaviorsBundle\Entity\CommentRecord;
use Sco\BehaviorsBundle\Form\CommentForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentController
 * @package Sco\BehaviorsBundle\Controller
 */
class CommentController extends Controller
{
    /**
     * Render form to create a new entity Comment.
     *
     * @param $object
     * @param null $parentId
     * @param null $extra
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formAction($object = null, $parentId = null, $extra = null)
    {
        $cms = $this->getParameter('sco.behaviors_bundle.cms_front');
        $template = "@ScoBehaviorsBundle/$cms/error.html.twig";

        if(empty($object)) {
            return $this->render($template, ['message' => 'Missing argument or null passed.']);
        }

        if(!is_object($object)) {
            return $this->render($template, ['message' => 'The method get_class is not callable.']);
        }

        if(!is_callable([get_class($object), 'getId'])) {
            return $this->render($template, ['message' => 'The method getId is not callable.']);
        }

        /** @var CommentRecord $record */
        $record = $this->get('sco.behaviors_bundle.comment_service')->registryComment($object->getId(), get_class($object), $extra);
        /** @var Comment $comment */
        $comment = $record->getComment();

        $form = $this->createForm(CommentForm::class, $comment, [
            'action' => $this->generateUrl('sco_behaviors_bundle_comments_new', ['object_id' => $object->getId(), 'parent_id' => $parentId]),
            'comment_record' => $record,
        ]);

        $template = "@ScoBehaviors/$cms/comment/form.html.twig";

        return $this->render($template, array(
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Create a new Entity Comment.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request): Response
    {
        $data = $request->get('sco_behaviors_bundle_comment_form');
        $cms = $this->getParameter('sco.behaviors_bundle.cms_front');

        if(empty($data) || empty($request->get('object_id')) || empty($data['commentRecord'])) {
	        if($request->isXmlHttpRequest()) {
		        return new Response('Missing argument or null passed.', Response::HTTP_BAD_REQUEST);
	        }
	        
            $template = "@ScoBehaviorsBundle/$cms/error.html.twig";
            return $this->render($template, ['message' => 'Missing argument or null passed.']);
        }

        $objectId = $request->get('object_id');
        $parentId = $request->get('parent_id');
	
	    /** @var CommentRecord $record */
	    $record = $this->get('sco.behaviors_bundle.comment_service')->registryComment($objectId,
		    $data['commentRecord'], json_decode($data['extra']));
	    /** @var Comment $comment */
	    $comment = $record->getComment();

        $form = $this->createForm(CommentForm::class, $comment, [
            'action' => $this->generateUrl('sco_behaviors_bundle_comments_new', ['object_id' => $objectId, 'parent_id' => $parentId]),
            'comment_record' => $record,
        ]);

        $form->submit($request->request->get($form->getName()));

        if ($form->isValid()) {
            if(null !== $extra = $form->getData()->getCommentRecord()->getExtra()) {
                $comment->getCommentRecord()->setExtra(json_decode($extra));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);

            if(null !== $parentId) {
                $parent = $em->getRepository(Comment::class)->find($parentId);
                $parent->addChildren($comment);
            }

            $em->flush();

            if($request->isXmlHttpRequest()) {
                return new Response($comment->getId(), Response::HTTP_OK);
            }

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        $template = "@ScoBehaviors/$cms/comment/form.html.twig";

        return $this->render($template, array(
            'request' => $request,
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Return list of comment for the object passed it parameter.
     *
     * @param $object
     * @param bool $disableReplyOnComment Possibility to disable reply on comment true or false, default false
     * @param string|null $pattern
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($object = null, bool $disableReplyOnComment = false, string $pattern = null): Response
    {
        $cms = $this->getParameter('sco.behaviors_bundle.cms_front');
        $template = "@ScoBehaviors/$cms/error.html.twig";

        if(empty($object)) {
            return $this->render($template, ['message' => 'Missing argument or null passed.']);
        }

        if(!is_object($object)) {
            return $this->render($template, ['message' => 'The method get_class is not callable.']);
        }

        /** @var Comment[]|array $comments */
        $comments = $this->get('sco.behaviors_bundle.comment_service')->fetchComments($object, $pattern);

        $template = "@ScoBehaviors/$cms/comment/list.html.twig";

        return $this->render($template, [
            'comments' => $comments,
            'object' => $object,
            'limit' => $this->getParameter('sco.behaviors_bundle.reply_limit'),
            'disableReplyOnComment' => $disableReplyOnComment
        ]);
    }

    /**
     * Return statistic by year and class_name ordering by year desc like
     * array:2 [▼
     *      2018 => array:1 [▼
     *          0 => array:3 [▼
     *              "class_name" => "PostBundle\Entity\Post"
     *              "class_total" => 1
     *              "year" => "2018"
     *          ]
     *      ]
     *      2017 => array:1 [▼
     *          0 => array:3 [▼
     *              "class_name" => "PostBundle\Entity\Post"
     *              "class_total" => 2
     *              "year" => "2017"
     *          ]
     *      ]
     *   ]
     *
     * @param string $filter
     * @param string $order
     * @return Response
     */
    public function statisticAction($filter, $order): Response
    {
        $search[$filter] = $order;

        /** @var array */
        [$key, $stats] = $this->get('sco.behaviors_bundle.comment_service')->fetchStatistics($search, $filter);

        $cms = $this->getParameter('sco.behaviors_bundle.cms_front')?? 'bootstrap';
        $template = "@ScoBehaviors/$cms/comment/statistic.html.twig";

        return $this->render($template, [
            'func' => $key,
            'stats' => $stats,
        ]);
    }
}
